package cz.cvut.fit.niadp.mvcgame.config;

public class MvcGameConfig {

    private MvcGameConfig(){

    }

    public static final int MAX_X = 1920;
    public static final int MAX_Y = 1080;

}